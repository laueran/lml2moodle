# extracted from schulzeug
# this script reads user and class information from an linuxmuster.net ldap
# and uses the moodle REST-API to create courses and enrols the users to the courses
# this is meant as a blueprint for the workflow and can easily be adopted

# run this script from any machine having ldap access to the ldap and
# web access to the moodle installation

# apt install python3-ldap

role_teacher = 'EDIT_ME' #  the script needs to know the role_id for teacher
role_student = 'EDIT_ME'        #  and student for enroling users to courses

cat_students = 'Schüler'        # names for the basic categories in moodle
cat_teachers = 'Lehrer'

rid_students = EDIT_ME                # to enrol users to a course, the global role id is
rid_teachers = EDIT_ME                # needed - there seems to be no rest-ish way to get it

teachersroom = 'teachers'   # well... its quite clear, isn't it?

# machine accounts, adminsitrators, ... will be filtered by gecos data field (maybe there is a better way?)
gecos_exclude = ['Domain Admin', 'Programm Administrator', 'Web Administrator', 'Administrator', 'administrator', 'LINBO Administrator', 'ExamAccount']

# the key, base-url and endpoint for the moodle REST api
KEY = 'EDIT_ME'
URL = 'EDIT_ME'
ENDPOINT = '/webservice/rest/server.php'

ldap_server = 'EDIT_ME'
ldap_binddn = '' # not used so far
ldap_bindpw = '' # anonymous bind
ldap_basedn = 'ou=accounts, dc=linux, dc=lokal'

# i read to this point, understood everything and really, really want to try this with my real moodle (True)
# i am still not understanding half of this terrible script and therefore just want to watch what it does (False)
iknowwhatido = False

# -----------------------------------------------------------------

import subprocess
import sys
import ldap
import re
import requests, json

class ldapuser:
    def __init__(self, uid, sn, givenName, mail, cl):
        self.uid = str(uid)
        self.sn = str(sn)
        self.givenName = str(givenName)
        self.mail = str(mail)
        self.cl = str(cl)

def getclassgroup(name):
    g = re.match('.*?([0-9]+)', name)
    if g == None:
        if name == 'teachers':
            return cat_teachers
        else:
            return cat_students
    else:
        return g.group(0)

def getldapuser():
    """ reads the linuxmuster.net users from the server ldap """
    l = ldap.initialize(ldap_server)                         # initialize the ldap object
    binddn = ldap_binddn       # not used for my linuxmuster.net server
    pw = ldap_bindpw
    basedn = ldap_basedn
    searchFilter = "(objectclass=person)"
    searchAttribute = []                                                # TODO: change this after testing
    searchScope = ldap.SCOPE_SUBTREE                                    # scope entire subtree
    try:
        ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_NEVER)         # ssl cert ignore                # the ldap bind
        l.protocol_version = ldap.VERSION3
        l.simple_bind_s()                                               # if bind user exists, use l.simple_bind_s(binddn, pw)
    except ldap.INVALID_CREDENTIALS:
      print ("Username or password is incorrect.")
      sys.exit(0)
    except ldap.LDAPError as e:                                         # ldap bind failure
      if type(e.message) == dict and e.message.has_key('desc'):
          print (e.message['desc'])
      else:
          print (e)
      sys.exit(0)

    try:
        ldap_result_id = l.search(basedn, searchScope, searchFilter, searchAttribute)
        result_set = []
        while True:
            result_type, result_data = l.result(ldap_result_id, 0)
            if (result_data == []):
                break
            else:
                if result_type == ldap.RES_SEARCH_ENTRY:
                    a, d = result_data[0]
                    # filter the admin-, exam- and machine-accounts
                    if d['gecos'][0].decode() not in gecos_exclude:
                        uid = d['uid'][0].decode()
                        sn = d['sn'][0].decode()
                        givenName = d['givenName'][0].decode()
                        mail = d['mail'][0].decode()
                        homeDirectory = d['homeDirectory'][0].decode().split('/')  # use 'homedirectory' for getting the class
                        if homeDirectory[2] == 'teachers':
                            cl = 'teachers'
                        elif homeDirectory[2] == 'students':
                            cl = homeDirectory[3]
                        else:
                            cl = False
                        result_set.append(ldapuser(uid, sn, givenName, mail, cl))
                        print(givenName + ' ' + sn + ' <' + mail + '>');
    except ldap.LDAPError as e:
        print (e)
    l.unbind_s()

    return result_set


# part one - read the user from linuxmuster.net-LDAP

ldapusers = getldapuser()                              # get all user

# part two - write the structure to moodle
# first go to https://<your-moodle-address>/admin/category.php?category=webservicesettings
# you need to create a user/token for the functions used beneath
# - core_course_create_categories
# - core_course_get_categories
# - core_course_create_courses
# -
# to create and users and courses and enrol courses



# do NOT do this to an existing moodle with courses that might conflict with
# the ones the script tries to create - this is meant for an innocent, new moodle
# and has not yet been tested at all!!
# !!!!!!!!! you have been warned !!!!!!!!!!!!!!!!!!!


# api documentation is here: https://<your-moodle-address>/admin/webservice/documentation.php


class moodle_role:
    def __init__(self, id, name):
        self.id = id
        self.name = name

def rest_api_parameters(in_args, prefix='', out_dict=None):
    """Transform dictionary/array structure to a flat dictionary, with key names defining the structure. """
    if out_dict==None:
        out_dict = {}
    if not type(in_args) in (list,dict):
        out_dict[prefix] = in_args
        return out_dict
    if prefix == '':
        prefix = prefix + '{0}'
    else:
        prefix = prefix + '[{0}]'
    if type(in_args)==list:
        for idx, item in enumerate(in_args):
            rest_api_parameters(item, prefix.format(idx), out_dict)
    elif type(in_args)==dict:
        for key, item in in_args.items():
            rest_api_parameters(item, prefix.format(key), out_dict)
    return out_dict

def call(fname, **kwargs):
    """Calls moodle API function with function name fname and keyword arguments. """
    parameters = rest_api_parameters(kwargs)
    parameters.update({"wstoken": KEY, 'moodlewsrestformat': 'json', "wsfunction": fname})
    
    response = requests.post(URL+ENDPOINT, data=parameters).json();
    if type(response) == dict and response.get('exception'):
        raise SystemError("Error calling Moodle API\n", response)
    return response

def category_create(name, parentid):
    """ Create Categories with 'name' and 'parentid' """
    return call('core_course_create_categories', categories = [{'name': name, 'parent': parentid, 'idnumber': name.lower()}])

def category_getid(name):
    """ get the id of a given category """

    res = call('core_course_get_categories', criteria = [{'key': 'name', 'value': name}])
    if res != []:
        return res[0]['id']
    else:
        try:
            return category_getid(cat_students)
        except:
            return None

def course_create(name, categoryid):
    """ create a course with name and category-id """
    mcourses = []
    mcourse = {}
    mcourse['fullname'] = name
    mcourse['shortname'] = name
    mcourse['categoryid'] = categoryid
    mcourses.append(mcourse)
    return call('core_course_create_courses', courses = mcourses)

def course_getid(name):
    print(name)
    """ returns course id or None"""
    crs_list = call('core_course_get_courses_by_field', field = 'shortname', value = name)['courses']
    if len(crs_list) == 0:
        print(f'No course found with shortname {name}!')
    elif len(crs_list) == 1:
        return crs_list[0]['id']
    else:
        print(f'Shortname {name} not unique. There are {len(crs_list)} courses with this shortname.')
    return None

def user_create(users):
    """ Create moodle-user from a list of ldap-users """
    musers = []
    for u in users:
        muser = {}
        muser['username'] = u.uid
        muser['firstname'] = u.givenName
        muser['lastname'] = u.sn
        muser['email'] = u.mail
        muser['auth'] = 'ldap'
        musers.append(muser)
    return call('core_user_create_users', users = musers)

def enrol_user(ids, users):
    """ manually enroles users to a course """
    musers = []
    for u in users:
        user = next(x for x in ids if x['username'] == u.uid)
        muser = {}
        if u.cl == 'teachers':
            muser['roleid'] = rid_teachers
        else:
            muser['roleid'] = rid_students

        muser['userid'] = user['id']
        muser['courseid'] = course_getid(u.cl)
        musers.append(muser)
    return call('enrol_manual_enrol_users', enrolments = musers)


# create to main categories in moodle ('Schüler' and 'Lehrer')
for cn in [cat_teachers, cat_students]:
    print ("* create basic category %s" % cn)
    if iknowwhatido:
        category_create(cn, 0)
    else:
        print("   -> category_create(%s, 0)" % cn)


# create the classgroup-categories

for cg in sorted(set([getclassgroup(u.cl) for u in ldapusers if getclassgroup(u.cl) != "root"])):
    print ("* create subcategory", cg)
    categoryid = category_getid(cat_students)
    if iknowwhatido:
        category_create('Klasse '+ cg, categoryid)
    else:
        print("   -> category_create(%s, %d)" % (cg, categoryid))

# create the courses in the classgroup-categories (and a teachers course in teachers category)
# every course that is not in category teachers OR any know subcategory will be in students subcat.
single = list(set(sorted(set([u.cl for u in ldapusers]))))
for c in single:
    if c == "teachers":
        categoryid = category_getid(cat_teachers)
        coursename = teachersroom
    else:
        categoryid = category_getid(getclassgroup(c))
        coursename = c
    print("* create course %s in category %s (%d)" % (c, getclassgroup(c), categoryid))
    if iknowwhatido:
        course_create(coursename, categoryid)
    else:
        print("   -> course_create(%s, %d)" % (coursename, categoryid))


for i in range(0, len(ldapusers), 20):
    # next we create user (directly from ldap-users)
    print("* create moodle users from ldapusers")
    result = None
    if iknowwhatido:
        result = user_create(ldapusers[i:i+20])
    else:
        print("   -> user_create(%s)" % 'list of ldap-users')

    # (manually) enrol users to courses
    # check rolename variables!
    # in my idea it is better to create cohorts and enrol cohorts to courses
    # but so far this needs a plugin and some more logic

    print("* enrol users to their courses")
    if iknowwhatido:
        enrol_user(result, ldapusers[i:i+20])
    else:
        print("   -> user_create(%s)" % 'list of ldap-users')

print('==========DONE==========')
